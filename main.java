
public class Main
{
	public static void main(String[] args) {
	    int max = 100;
	    int temp;
		int mass[] = new int[max + 1];
		for (int i = 1; i <= max; i++) {
		    temp = i % 2;
		    if (temp == 0) {
		        mass[i] = i;
		        System.out.print(mass[i] + " ");
		    }
		}
	}
}
